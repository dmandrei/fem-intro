#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 21:56:22 2020

@author: dandy
"""

import numpy as np
import matplotlib.pyplot as plt

def gnummake(de, dn):
  gnum = np.zeros((de[1], 4, (dn[0]-1)))
  for i in range(de[1]):  
    #numbering along x. If you want to change - change the indes of dn
    gnum[i] = np.array([range(    i*dn[0]    , (i+1)*dn[0]-1),
                        range((i+1)*dn[0]    , (i+2)*dn[0]-1),
                        range((i+1)*dn[0] + 1, (i+2)*dn[0]  ),
                        range(    i*dn[0] + 1, (i+1)*dn[0]  )])
  return np.concatenate(gnum, axis=1).astype(int).T

def nodalcoords(xs, ys):
  coords = [[[xs[i],ys[j]] for i in range(len(xs))] for j in range(len(ys))]
  return np.concatenate(coords)

def plot_grid(ncrd):
  plt.figure(figsize=(12,8))
  plt.plot(ncrd[:,0],ncrd[:,1], 'o',color = 'k', markersize=5)

#Shape functions for a bilinear 2D quadrilateral element:
def N1(eps, eta):
  return 0.25 * (1 - eps) * (1 - eta)
def N2(eps, eta):
  return 0.25 * (1 - eps) * (1 + eta)
def N3(eps, eta):
  return 0.25 * (1 + eps) * (1 + eta)
def N4(eps, eta):
  return 0.25 * (1 + eps) * (1 - eta)
#Derivatives
def dN1dE(eta):
  return -0.25 * (1 - eta)
def dN2dE(eta):
  return -0.25 * (1 + eta)
def dN3dE(eta):
  return  0.25 * (1 + eta)
def dN4dE(eta):
  return  0.25 * (1 - eta)

def dN1dn(eps):
  return -0.25 * (1 - eps)
def dN2dn(eps):
  return  0.25 * (1 - eps)
def dN3dn(eps):
  return  0.25 * (1 + eps)
def dN4dn(eps):
  return -0.25 * (1 + eps)

def J(eps, eta, crd):
  a = np.array([[dN1dE(eta), dN2dE(eta), dN3dE(eta), dN4dE(eta)],
                [dN1dn(eps), dN2dn(eps), dN3dn(eps), dN4dn(eps)]])  
  return a.dot(crd)

def Mget(rho, cp, crd):
  refps = [-np.sqrt(1/3.), np.sqrt(1/3.)]
  wts   = [1.,1.]
  result = 0
  for eta in refps:
    for eps in refps:
      N = np.array([[N1(eps, eta), N2(eps, eta), N3(eps, eta), N4(eps, eta)]])
      result += N.T.dot(N) * np.linalg.det(J(eps, eta, crd)) * wts[0] * wts[1]
  return result * rho * cp

def Kget(D, crd): #D - conductivity matrix
  assert D.shape == (2,2)
  refps = [-np.sqrt(1/3.), np.sqrt(1/3.)]
  wts   = [1.,1.]
  result = 0
  for eta in refps:
    for eps in refps:
      gradN = np.array([[dN1dE(eta), dN2dE(eta), dN3dE(eta), dN4dE(eta)],
                        [dN1dn(eps), dN2dn(eps), dN3dn(eps), dN4dn(eps)]])
      result += gradN.T.dot(D).dot(gradN) * np.linalg.det(J(eps, eta, crd)) * wts[0] * wts[1]
  return result

def Fget(src):
  refps = [-np.sqrt(1/3.), np.sqrt(1/3.)]
  wts   = [1.,1.]
  result = 0
  for eta in refps:
    for eps in refps:
      N = np.array([[N1(eps, eta), N2(eps, eta), N3(eps, eta), N4(eps, eta)]])
      result += N.T.dot(src)
  return result

