#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 12:39:49 2020

@author: dandy
"""

def analytical_solution(x, t=0., kappa=1.,Tmax=100., sigma=1.):
  return Tmax / np.sqrt(1 + 4*t*kappa/sigma**2) * np.exp(-x**2 / (sigma**2 + 4*t*kappa))

def N1(eps, L):
  return 1/2.*(1 - eps)

def N2(eps, L):
  return 1/2.*(1 + eps)

def quad(L, naccuracy = 2, output='M', func1 = N1, func2 = N2):
  if naccuracy == 2:
    eps = [-np.sqrt(1/3.), np.sqrt(1/3.)]
    wts = [1.            , 1.]
  elif naccuracy == 3:
    eps = [-0.774596669241483, 0.               , 0.774596669241483]
    wts = [ 0.555555555555556, 0.888888888888889, 0.555555555555556]
  
  integral = 0.
  
  if   output == 'M':
    for i in range(len(eps)):
      integral += func1(eps[i], L) * func2(eps[i], L) * L/2. * wts[i]

  elif output == 'K11' or output == 'K22':
    for i in range(len(eps)):
      integral += 2./L * 1/4. * wts[i]
  
  elif output == 'K12' or output == 'K21':
    for i in range(len(eps)):
      integral += 2./L * (-1/4.) * wts[i]
      
  elif output == 'F':
    for i in range(len(eps)):
      integral += func1(eps[i], L) * L/2. * wts[i]
      
  return integral

def Mget(L, naccuracy = 2,):
  M = np.array(
      [[quad(L, naccuracy, 'M', N1, N1), quad(L, naccuracy, 'M', N1, N2)],
       [quad(L, naccuracy, 'M', N2, N1), quad(L, naccuracy, 'M', N2, N2)]])
  return M

def Kget(L, naccuracy = 2,):
  K = np.array(
      [[quad(L, naccuracy, 'K11'), quad(L, naccuracy, 'K12')],
       [quad(L, naccuracy, 'K12'), quad(L, naccuracy, 'K22')]])
  return K

def Fget(L, naccuracy = 2,):
  F = np.array(
      [[quad(L, naccuracy, 'F', N1), quad(L, naccuracy, 'F', N2)]])
  return F


import numpy as np
