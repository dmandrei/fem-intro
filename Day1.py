#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 11:53:24 2020
@author: d.andy
"""
def analytical_solution(x, t=0., kappa=1.,Tmax=100., sigma=1.):
  return Tmax / np.sqrt(1 + 4*t*kappa/sigma**2) * np.exp(-x**2 / (sigma**2 + 4*t*kappa))

import numpy as np
import matplotlib.pyplot as plt
# 1.Define all physical parameters:
  # Diffusivity, source term, length of spatial domain
  # .. and numerical parameters: number of elements and nodes
ne    = 45     # number of elements
nn    = ne+1   # number of nodes

L     =  10.  # domain length
s     = -0.  # source term

# tstep = 2.   # timestep
# tmax  = 20.  # model time
# t     = np.arange(0, tmax, tstep)

t =  np.logspace(0,1.5, 20)
tsteparr = [t[i] - t[i-1] for i in range(1,len(t))]

kappa = [1.]*ne # thermal diffusivity
#kappa[6:13] = np.ones(7)*0.0001
assert len(kappa) == ne

# 2.Define the spatial coordinates xi for your nodes, and specify the time domain 
# where you want to obtain the discrete solution.

x = list(np.linspace(-5,-2, 5)) + list(np.linspace(2, 5, 5)) + list(np.linspace(-1.9, 1.9, nn-10))
x = sorted(x)
assert len(x) == nn
dxarr = [(x[i] - x[i-1]) for i in range(1, len(x))]

#T     = np.array([int(nn/2)*[100,1]]).T
#T = np.array([[100]]*nn)
sigma = 1.
Tmax  = 100.
T = np.array([[Tmax*np.exp(-xx**2/sigma**2) for xx in x]]).T
assert len(T) == nn
plt.figure(figsize=(15,10))
plt.plot(x,T,'o-', label = 'init')


# 3.Define the relationship between the element node numbers 
# and the global node numbers

g_num = np.array([range(0,nn-1),range(1,nn)])  

# for example, g_num[0,2], g_num[1,2] - number of 0 and 1 nodal points of element #2 in global numbering

# 4. Define boundary node indices and the values of the potential (e.g., temperature)
# at the boundary nodes.

bcdof = [0, nn-1]
bcval = [0., 0.]

# 5. Initialise the global matrices LG, RG and FG so that their dimensions are defined
# and that the matrices are filled with zeros.


# 6. Within an element loop, define the element matrices M and K and the element
# load vector F (see Equations (3.2), (3.3) and (3.4)). Use these to compute L
# and R. Sum these matrices (and F) node-by-node to form the global stiffness
# matrices LG and RG and the global vector FG (see Equation (3.9)). If the element properties do not depend on time these global matrices only need to be
# calculated once and can be saved for later use.

for itm in range(len(t)):
  LG, RG, FG  = np.zeros((nn,nn)), np.zeros((nn,nn)), np.zeros((nn,1))
  tstep = tsteparr[itm]
  for iel in range(ne):
    dx = dxarr[iel]
    M = np.array([[ dx/3., dx/6.], 
                  [ dx/6., dx/3.]])
    F = s * np.array([[ dx/2., dx/2. ]]).T
    R = M / tstep
    K = kappa[iel] * np.array([[ 1/dx, -1/dx],
                               [-1/dx,  1/dx]])
    L = M / tstep + K
    
    index = (slice(g_num[0,iel],(g_num[1,iel]+1)), 
             slice(g_num[0,iel],(g_num[1,iel]+1)))
    LG[index] += L
    RG[index] += R
    FG[g_num[:, iel]] += F
    
  # 7. Within a time loop, perform the operations on the right hand side of Equation
  # (3.9) (i.e., firstly multiply RG with the old temperature vector Tn and then add the
  # resulting vector to FG to form the right hand side vector b). Note that the vector
  # T0 must contain the discrete form of your initial condition, T(x, 0).
  B = RG.dot(T) + FG
    
  # 8. Apply boundary conditions
  for i in range(len(bcdof)):
    LG[bcdof[i],:] = 0.
    LG[bcdof[i],bcdof[i]] = 1.
    B [bcdof[i]] = bcval[i]

  # 9. Solve Equation (3.9) for the new temperature, and then continue to the next time
  # step.
  T = np.linalg.solve(LG, B)
  plt.plot(x,T,'o-', label = str(t[itm]), markersize=5 )


#analytical
y = np.array([analytical_solution(xx, t=t[-1], Tmax = Tmax,  sigma=sigma) for xx in x])
plt.plot(x, y, label='analytical', linestyle='--', linewidth=3, color='k')

plt.legend()
