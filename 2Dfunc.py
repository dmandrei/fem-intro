#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 21:56:22 2020

@author: dandy
"""

def gnummake(de, dn):
  gnum = np.zeros((de[1], 4, (dn[0]-1)))
  for i in range(de[1]):  
    #numbering along x. If you want to change - change the indes of dn
    gnum[i] = np.array([range(    i*dn[0]    , (i+1)*dn[0]-1),
                        range((i+1)*dn[0]    , (i+2)*dn[0]-1),
                        range((i+1)*dn[0] + 1, (i+2)*dn[0]  ),
                        range(    i*dn[0] + 1, (i+1)*dn[0]  )])
  return np.concatenate(gnum, axis=1)

def nodalcoords(xs, ys):
  coords = [[[xs[i],ys[j]] for i in range(dn[0])] for j in range(dn[1])]
  return np.concatenate(coords)

def plot_grid(ncrd):
  plt.figure(figsize=(12,8))
  plt.plot(ncrd[:,0],ncrd[:,1], 'o',color = 'green', markersize=5)

#Shape functions for a bilinear 2D quadrilateral element:
def N1(eps, eta):
  return 0.25 * (1 - eps) * (1 - eta)
def N2(eps, eta):
  return 0.25 * (1 - eps) * (1 + eta)
def N3(eps, eta):
  return 0.25 * (1 + eps) * (1 + eta)
def N4(eps, eta):
  return 0.25 * (1 + eps) * (1 - eta)
#Derivatives
def dN1dE(eta):
  return -0.25 * (1 - eta)
def dN2dE(eta):
  return -0.25 * (1 + eta)
def dN3dE(eta):
  return  0.25 * (1 + eta)
def dN4dE(eta):
  return  0.25 * (1 - eta)

def dN1dn(eps):
  return -0.25 * (1 - eps)
def dN2dn(eps):
  return  0.25 * (1 - eps)
def dN3dn(eps):
  return  0.25 * (1 + eps)
def dN4dn(eps):
  return -0.25 * (1 + eps)

def integration():
  refps = [-np.sqrt(1/3.), np.sqrt(1/3.)]
  wts   = [1.,1.]
  
#def J(local):
  