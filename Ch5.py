#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 18:14:37 2020

@author: dandy
"""
from func2D import *

plt.style.use(['seaborn-deep'])

de = tuple([12, 11]) #shape of the elemental grid
L  = tuple([100.,60.])
assert len(de) == len(L)
nnperel = 4 #number of nodes per elements

dn = tuple([ int(d*((nnperel)**0.5 - 1) + 1) for d in de]) #shape of the nodal grid
ne = de[0] * de[1] #number of elements in x and y directions
nn = dn[0] * dn[1] #number of nodes in both dir-s

times = np.linspace(0,1, 10)
tsteps = times[1:] - times[:-1]

# homogeneous lateral step
ls = tuple([L[i]/de[i] for i in range(len(de))]) 
xs, ys = np.linspace(0, L[0], dn[0]), np.linspace(0, L[1], dn[1])
#nodal coordinates
ncrds = nodalcoords(xs, ys)
#reference-global matrix numbering translation:
gnum = gnummake(de,dn)
#have a look at the grid
if 1: plot_grid(ncrds)

#boundary conditions:
unique, counts = np.unique(gnum, return_counts=True)
bcdof = unique[counts <= 2]
bcval = [0.]*len(bcdof)



#thermal conductivity nodal tensors with unity everythere
kappa = np.ones(dn + tuple([2,2]))
assert kappa.shape[0:2] == dn
rho   = np.ones(de)*3000. #density
Cp    = np.ones(de)*1000. #thermal capacity

dens = 1.
cpel  = 1.
kappa = np.array([[10., 0],
                  [0, 10.]])
source = 0.
#T = np.array([[100]*dn[0], [80]*dn[0], [60]*dn[0], [40]*dn[0], [20]*dn[0]])
T = np.ones(dn)*1000.

T = T.reshape(nn, 1)

LG = np.zeros((nn, nn))
RG = np.zeros((nn, nn))
FG = np.zeros((nn, 1))

for tstep in tsteps:
  for eln in range(ne):
    gnuel  = gnum[eln]
    crdel  = ncrds[gnuel]
    M = Mget(dens,  cpel, crdel)
    K = Kget(kappa, crdel)
    L = M / tstep + K
    R = M / tstep
    F = Fget(source)
    
    index = tuple(np.meshgrid(gnuel, gnuel))#[::-1])
    LG[index] += L
    RG[index] += R
    FG[gnuel] += F
    
  B = RG.dot(T) + FG
  print(np.mean(LG), np.mean(RG), np.mean(FG))
  # 8. Apply boundary conditions
  for i in range(len(bcdof)):
    LG[bcdof[i],:] = 0.
    LG[bcdof[i],bcdof[i]] = 1.
    B [bcdof[i]] = bcval[i]
    
  # 9 Solve the linear system
  T = np.linalg.solve(LG, B)
  print(np.mean(T), np.max(T))

Tplot = T.reshape((dn[1], dn[0]))
plt.contourf(xs, ys, Tplot)
plt.colorbar()
