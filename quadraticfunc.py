#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 15:48:59 2020

@author: dandy
"""
import numpy as np 
import sympy as sp

def analytical_solution(x, tt=0., kappa=1.,Tmax=100., sigma=1.):
  return Tmax / np.sqrt(1 + 4*tt*kappa/sigma**2) * np.exp(-x**2 / (sigma**2 + 4*tt*kappa))

def N1(eps):
  return 1/2.* eps * (eps - 1)
def N2(eps):
  return 1 - eps**2
def N3(eps):
  return 1/2.* eps * (eps + 1)

def dN1(eps):
  return eps - 0.5
def dN2(eps):
  return eps * (-2)
def dN3(eps):
  return eps + 0.5

def quad(L, output, f1=N1, f2=N1):
  epss = [-0.774596669241483, 0.               , 0.774596669241483]
  wtss = [ 0.555555555555556, 0.888888888888889, 0.555555555555556]
  integral = 0.
  
  if   output == 'M':
    for i in range(len(epss)):
      integral += L/2. * f1(epss[i]) * f2(epss[i]) * wtss[i]
    
  elif output == 'K':
    for i in range(len(epss)):
      integral += 2./L * f1(epss[i]) * f2(epss[i]) * wtss[i]

  elif output == 'F':
    for i in range(len(epss)):
      integral += L/2. * f1(epss[i]) * wtss[i]
  
  return integral

def Mget(L):
  fs= [N1,N2,N3]
  return np.array([[quad(L, 'M', f1, f2) for f1 in fs] for f2 in fs])

def Kget(L):
  fs= [dN1,dN2,dN3]
  return np.array([[quad(L, 'K', f1, f2) for f1 in fs] for f2 in fs])

def Fget(L):
  fs= [N1,N2,N3]
  return np.array([[quad(L, 'F', f) for f in fs]])

